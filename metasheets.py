#!/usr/bin/env python3

"""
Read google sheets with dhis2 metadata and create a json file that you
can upload to a dhis2 instance.
"""

import sys
import os
from itertools import groupby
from collections import namedtuple
import hashlib
import json
from argparse import ArgumentParser
from configparser import ConfigParser, ParsingError

from googleapiclient.discovery import build, HttpError

Sheet = namedtuple('Sheet', ['api', 'id'])


def main():
    try:
        args = get_args()

        sheet = Sheet(
            api=build('sheets', 'v4', developerKey=args.api_key).spreadsheets(),
            id=args.sheet_id)

        log(f'Reading https://docs.google.com/spreadsheets/d/{sheet.id} ...')

        title, pages = read_data(sheet)
    except (HttpError, AssertionError) as e:
        sys.exit(e)

    defaultc = args.default_category_combo_id  # shortcut
    metadata = {
        'dataSets': get_dataSets(pages, defaultc),
        'sections': get_sections(pages),
        'dataElements': (get_dataElements(pages, defaultc) +
                         get_programDataElements(pages)),
        'programs': get_programs(pages, defaultc),
        'programStages': get_programStages(pages),
        'programSections': get_programSections(pages),
        'trackedEntityTypes': get_trackedEntityTypes(pages),
        'trackedEntityAttributes': get_trackedEntityAttributes(pages),
        'categoryCombos': get_categoryCombos(pages),
        'categories': get_categories(pages),
        'categoryOptions': get_categoryOptions(pages),
        'optionSets': get_optionSets(pages),
        'options': get_options(pages)}

    output_metadata = args.output or (output_name(title) + '.json')
    if not args.overwrite:
        check_if_exists(output_metadata)

    log(f'Writing metadata to file: "{output_metadata}" ...')
    with open(output_metadata, 'wt') as fout:
        json.dump(metadata, fout)

    output_ids = output_name(title) + '_ids.txt'
    if not args.overwrite:
        check_if_exists(output_ids)

    log(f'Writing ids to file: "{output_ids}" ...')
    with open(output_ids, 'wt') as fout_ids:
        write_ids(pages, sheet.id, fout_ids)


# Configuration.

def get_args():
    "Return arguments, with defaults read from config file if given"
    parser = ArgumentParser(description=__doc__, allow_abbrev=False)

    add = parser.add_argument  # shortcut
    add('--output',
        help='output file (if empty, it is generated from the sheet id)')
    add('--overwrite', action='store_true',
        help='do not check if the output file already exists')
    add('--api-key', help='api key to access google sheets')
    add('--sheet-id', help='id of the google sheet to read')
    add('--default-category-combo-id',
        help='id of the default category combo in the target dhis2 instance')
    add('--config', default='default.conf', help='file with default parameters')
    args = parser.parse_args()

    if args.config:
        try:
            cfg = read_config(args.config)
            check_config(cfg, args)
            update_args(args, cfg)
        except (FileNotFoundError, AssertionError,
                ValueError, ParsingError) as e:
            sys.exit('Error in file %s: %s' % (args.config, e))

    assert args.api_key and args.sheet_id, \
        f'Api key and sheet id are needed. Try: {sys.argv[0]} -h'

    assert is_valid_id(args.default_category_combo_id), \
        ('Default category id is not a valid dhis2 id: %s' %
         args.default_category_combo_id)

    return args


def read_config(fname):
    "Return dict with the parameters read from configuration file fname"
    log(f'Reading defaults from config file "{fname}" ...')
    cp = ConfigParser()
    cp.read_file(open(fname))
    assert 'metasheets' in cp, 'Missing section [metasheets]'
    return cp['metasheets']


def check_config(cfg, args):
    "Assert all the keys in configuration dict cfg exist in args"
    valid_keys = dict(args._get_kwargs()).keys() - {'config'}
    for key in cfg:
        assert key.replace('-', '_') in valid_keys, 'Unknown option "%s"' % key


def update_args(args, cfg):
    "Modify args with the contents of config dict cfg"
    used_keys = {x[2:] for x in sys.argv if x.startswith('--')}
    for key in cfg.keys() - used_keys:
        setattr(args, key.replace('-', '_'), cfg[key])


# Read spreadsheet data.

def read_data(sheet):
    pages_needed = [
        'dataSets', 'sections', 'dataElements',
        'programs', 'programStages', 'programSections', 'programDataElements',
        'trackedEntityTypes', 'trackedEntityAttributes',
        'categoryCombos', 'categories', 'categoryOptions',
        'optionSets', 'options']

    title, pages_spreadsheet = get_info(sheet)
    print(f'Found spreadsheet: "{title}"')

    check_pages(pages_spreadsheet, pages_needed)
    check_version(sheet)

    log('Reading spreadsheet values...')
    return title, {page: get_values(sheet, page) for page in pages_needed}


def get_info(sheet):
    info = sheet.api.get(spreadsheetId=sheet.id).execute()
    title = info['properties']['title']
    pages_spreadsheet = [x['properties']['title'] for x in info['sheets']]
    return title, pages_spreadsheet


def check_pages(pages_spreadsheet, pages_needed):
    "Inform about extra pages and assert there are no missing ones"
    extra_pages = list(set(pages_spreadsheet) - set(pages_needed) - {'DHIS2'})
    if extra_pages:
        warn('Pages in the spreadsheet that we do not use:\n  ' +
             ', '.join(f'"{page}"' for page in extra_pages))

    missing_pages = list(set(pages_needed) - set(pages_spreadsheet))
    assert not missing_pages, \
        f'Pages needed but not in the spreadsheet: {missing_pages}'


def check_version(sheet):
    response = sheet.api.values().get(spreadsheetId=sheet.id,
                                      range='DHIS2!B1').execute()
    version = response['values'][0][0]
    assert version in ['0.9', '1.0'], \
        f'Unknown version of spreadsheet (at DHIS2 B1): "{version}"'


def get_values(sheet, page):
    "Return the values of the given spreadsheet's page, as a list of dicts"
    xs = sheet.api.values().get(spreadsheetId=sheet.id,
                                range=page+'!A:ZZ').execute()
    xs = to_dicts(xs['values'])
    xs = fill_empty_ids(xs, page)
    xs = remove_empty_keys(xs)
    return xs


def to_dicts(rows):
    """Return a list of dicts with keys taken from the first row

    rows is a list of row values as returned by the google spreadsheet api.

    Example: [['id', 'name'], ['x1', 'n1'], ['x2']] ->
               [{'id': 'x1', 'name': 'n1'}, {'id': 'x2', 'name': ''}]
    """
    names = rows[0]
    value_rows = [row + ([''] * (len(names) - len(row))) for row in rows[1:]]
    return [dict(zip(names, values)) for values in value_rows]


def fill_empty_ids(xs, page):
    "Return dicts but with the empty id fields filled with a valid dhis2 id"
    def seed(x):
        postfix = x['optionSet'] if 'page' == 'options' else ''
        return page + x['name'] + postfix
    return [x if x['id'] else dict(x, id=create_id(seed(x))) for x in xs]


def create_id(text):
    "Return a valid dhis2 id based on the given text"
    # A valid id is 1 letter and 10 alphanumerics: [A-Za-z][A-Za-z0-9]{10}
    alpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    alnum = alpha + '0123456789'

    hexhash = hashlib.md5(text.encode('utf-8')).hexdigest()
    nums = [int(hexhash[2*n:2*n+2], 16) for n in range(1 + 10)]

    return (alpha[nums[0] % len(alpha)] +
            ''.join(alnum[n % len(alnum)] for n in nums[1:]))


def is_valid_id(uid):
    alpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    alnum = alpha + '0123456789'
    return (type(uid) == str and len(uid) == 11
            and uid[0] in alpha and all(c in alnum for c in uid[1:]))


def remove_empty_keys(xs):
    "Return dicts without the keys that have empty values"
    xs_stripped = []
    for x in xs:
        x_stripped = {}
        for k, v in x.items():
            if v:
                x_stripped[k] = x[k]
        xs_stripped.append(x_stripped)
    return xs_stripped


# Transform to metadata as expected by dhis2.

def get_dataSets(pages, defaultc):
    xs = pages['dataSets']
    xs = [replace_to_id(x, 'categoryCombo', pages['categoryCombos'], defaultc) for x in xs]

    def points_to(x):
        name = x['name']
        return [e for e in pages['dataElements'] if
                get(pages['sections'], e['dataSetSection'])['dataSet'] == name]

    def to_mdata(elem, dset):
        ccombo = get(pages['categoryCombos'], elem.get('categoryCombo'))
        return {'dataSet': {'id': dset['id']},
                'dataElement': {'id': elem['id']},
                'categoryCombo': {'id': ccombo.get('id', defaultc)}}

    xs = [dict(x, dataSetElements=[to_mdata(p, x) for p in points_to(x)]) for x in xs]
    return xs

def get_sections(pages):
    xs = pages['sections']
    xs = add_sortOrder(xs, 'dataSet')
    xs = [replace_to_id(x, 'dataSet', pages['dataSets']) for x in xs]
    xs = add_pointing(xs, 'dataElements', pages['dataElements'], 'dataSetSection')
    return xs

def get_dataElements(pages, defaultc):
    xs = pages['dataElements']
    xs = [replace_to_id(x, 'categoryCombo', pages['categoryCombos'], defaultc) for x in xs]
    xs = [replace_to_id(x, 'optionSet', pages['optionSets']) for x in xs]
    xs = [dict(x, domainType='AGGREGATE') for x in xs]
    return xs

def get_programDataElements(pages):
    xs = pages['programDataElements']
    xs = [replace_to_id(x, 'optionSet', pages['optionSets']) for x in xs]
    xs = [dict(x, domainType='TRACKER') for x in xs]
    return xs

def get_programs(pages, defaultc):
    xs = pages['programs']
    xs = [replace_to_id(x, 'trackedEntityType', pages['trackedEntityTypes']) for x in xs]
    xs = [replace_to_id(x, 'categoryCombo', pages['categoryCombos'], defaultc) for x in xs]
    xs = add_pointing(xs, 'programStages', pages['programStages'], 'program')
    # TODO: add programSections, programTrackedEntityAttributes
    return xs

def get_programStages(pages):
    xs = pages['programStages']
    xs = add_sortOrder(xs, 'program')
    xs = [replace_to_id(x, 'program', pages['programs']) for x in xs]
    # TODO: add programStageDataElements
    return xs

def get_programSections(pages):
    xs = pages['programSections']
    xs = add_sortOrder(xs, 'programStage')

    def programStage_to_program(x):
        program_name = get(pages['programStages'], x['programStage'])['program']
        x_new = dict(x, program=program_name)
        x_new.pop('programStage')
        return x_new

    xs = [programStage_to_program(x) for x in xs]
    xs = [replace_to_id(x, 'program', pages['programs']) for x in xs]
    # TODO: add trackedEntityAttributes
    return xs

def get_trackedEntityTypes(pages):
    xs = pages['trackedEntityTypes']

    def to_mdata(attr):  # why does dhis2 make it so complicated?
        mdata = {'value': attr['id'],
                 'text': attr['name'],
                 'trackedEntityAttribute': {'id': attr['id']}}
        for field in ['searchable', 'mandatory', 'unique',
                      'displayInList', 'optionSet', 'valueType']:
            if field in attr:
                mdata[field] = attr[field]
        mdata = replace_to_id(mdata, 'optionSet', pages['optionSets'])
        return mdata

    xs = add_pointing(xs, 'trackedEntityTypeAttributes',
                      pages['trackedEntityAttributes'], 'trackedEntityType', to_mdata)
    return xs

def get_trackedEntityAttributes(pages):
    xs = pages['trackedEntityAttributes']
    xs = [replace_to_id(x, 'optionSet', pages['optionSets']) for x in xs]
    return xs

def get_categoryCombos(pages):
    xs = pages['categoryCombos']
    xs = add_pointing(xs, 'categories', pages['categories'], 'categoryCombo')
    return xs

def get_categories(pages):
    xs = pages['categories']
    xs = add_pointing(xs, 'categoryOptions', pages['categoryOptions'], 'category')
    return xs

def get_categoryOptions(pages):
    xs = pages['categoryOptions']
    return xs

def get_optionSets(pages):
    xs = pages['optionSets']
    xs = add_pointing(xs, 'options', pages['options'], 'optionSet')
    return xs

def get_options(pages):
    xs = pages['options']
    xs = add_sortOrder(xs, 'optionSet')
    xs = [replace_to_id(x, 'optionSet', pages['optionSets']) for x in xs]
    return xs


def get(xs, name):
    "Return the element x in xs whose x['name'] is the given name"
    return next((x for x in xs if x['name'] == name), {})


# Replacements.

def add_sortOrder(entries, field):
    "Return a list like the given entries but with a 'sortOrder' based on field"
    # [{'id': 'x', field: 'a'}, {'id': 'y', field: 'b'}, {'id': 'z', field: 'a'}
    # -> [{'id': 'x', field: 'a', 'sortOrder': 1},
    #     {'id': 'z', field: 'a', 'sortOrder': 2},
    #     {'id': 'y', field: 'b', 'sortOrder': 1},
    entries_with_sortOrder = []
    for _, grouped_entries in groupby(entries, lambda x: x[field]):
        entries_with_sortOrder += [dict(entry, sortOrder=i+1)
                                   for i,entry in enumerate(grouped_entries)]
    return entries_with_sortOrder


def replace_to_id(x, field, pointed_xs, default=''):
    """Return dict like x with the given field replaced by {'id': ...}

    Example: {field: fname, ...} -> {field: {'id': fid}, ...}
      with  fid == pointed_xs[i]['id']  when  fname == pointed_xs[i]['name']
    """
    if field not in x or not x[field]:
        if default:
            return dict(x, **{field: {'id': default}})
        else:
            return x
    else:
        return dict(x, **{field: {'id': get(pointed_xs, x[field])['id']}})


def add_pointing(xs, field_new, pointing, field_pointing, to_metadata=None):
    "Return a list of dicts like xs with field_new as a list of pointing data"
    # "pointing" is a list of dicts that have a key "field_pointing", that
    # can point to the name of one of the dicts in xs.
    to_metadata = to_metadata or (lambda p: {'id': p['id']})
    def pointing_ids(x):
        name = x['name']
        return [to_metadata(p) for p in pointing if p[field_pointing] == name]
    return [dict(x, **{field_new: pointing_ids(x)}) for x in xs]


# Output.

def output_name(name):
    "Return a valid ouput filename based on the given name"
    for c in '/ \n\t\r\\':
        name = name.replace(c, '_')
    return name


def check_if_exists(fname):
    if os.path.exists(fname):
        try:
            answer = input(f'File "{fname}" already exists. Overwrite? [y/n] ')
            assert answer.lower().startswith('y')
        except (KeyboardInterrupt, AssertionError):
            sys.exit('\nCancelling.')


def write_ids(pages, sheet_id, fout):
    fout.write(f"""This file was generated by metasheets.

It contains the names of the spreadsheet pages requested by the
script, each one followed by the ids that were assigned to each
element and their names, in the same order as they appear in the
original spreadsheet.

You can update the corresponding id columns at:
https://docs.google.com/spreadsheets/d/{sheet_id}""")

    for name,elements in pages.items():
        fout.write(f'\n\n\n{name}\n'
                   '-----------\n')
        fout.write('\n'.join(f'{e["id"]}  {e["name"]}' for e in elements))
    fout.write('\n')


def log(txt):
    print(blue(txt))

def warn(txt):
    print(magenta(txt))

def ansi(n):
    "Return function that escapes text with ANSI color n"
    return lambda txt: '\x1b[%dm%s\x1b[0m' % (n, txt)

black, red, green, yellow, blue, magenta, cyan, white = map(ansi, range(30, 38))



if __name__ == '__main__':
    main()
