# Metasheets

Read google sheets with dhis2 metadata and create a json file that you
can upload to a dhis2 instance.


## Quick examples

Getting help:

```sh
./metasheets.py --help
```

Specifying the basic options from the command line:

```sh
./metasheets.py --api-key $KEY --sheed-id $SID --default-category-combo-id $DID
```

Running with arguments read from a configuration file:

```sh
./metasheets.py --config mysheet.conf
```

The result is a json file with the name of the spreadsheet (unless you
choose a different one with `--output`), containing all the
metadata. It also writes a file with the ids assigned to each element.


## Description

We can **create or modify metadata** in dhis2 by simply using the
official `Maintenance` app. But sometimes this is a slow process.

Instead, we can specify the metadata in a spreadsheet with a specific
format, so we can manipulate it quickly, and when we are happy with it
just run this script. It will generate a json containing all the
information that dhis2 needs to update the metadata automatically.

To upload it to a dhis2 instance, use its `Import/Export` app, go to
`Metadata import` and use `Merge` as the "import strategy".

If dhis2 fails to automatically update all the category option
combinations (which happens occassionally and is a dhis2 issue), you
can use the `Data Administration` app, go to `Maintenance`, select
`Update category option combinations` and click on the `Perform
Maintenance` button.

You can freeze the ids and make sure that they are not changed in the
future, which is useful for elements that already exist in the dhis2
instance (but you may still want to modify at a later time). For that,
the script writes a file with all the ids that it assigned to each
element in the metadata file, so you can copy and paste them into the
original spreadsheet.

This code is inspired by
[GLASS-metdatada](https://github.com/EyeSeeTea/GLASS-metadata/) and
aims to provide a simpler implementation.


## Requirements

The only external requirement is the [google api python
client](https://github.com/googleapis/google-api-python-client), which
is included in most distributions, and for example can be added in
debian-based ones with:

```sh
sudo apt install python3-googleapi
```


## Configuration file

The optional argument `--config` specifies a configuration file. You
can write there all the arguments that the program needs to run.

The configuration file looks like:

```ini
[metasheets]
api-key = ...
sheet-id = ...
default-category-combo-id = ...
```

Arguments specified in the command line take precedence over the ones
in the configuration file (so if you call the program with
`--sheet-id`, that value is used instead of the one in the configuration
file).


## Default category combo

To get the default `categoryCombo` used at a dhis2 instance, go to the
following endpoint:
`/api/categoryCombos.json?filter=name:eq:default&fields=id,name` .

### How the default category combo is used

If you want to understand what is special about the default category
combination and how it works in dhis2, you can check [this talk by Jim
Grace](https://youtu.be/EcR9QwJvc7c?t=314) (and maybe [these
slides](https://drive.google.com/file/d/1MWq-Nx-AcSSuTfF9z7VPq0W9PXyl9IAn/view)).

Where is it used? Looking at exported metadata from the
[play.dhis2.org server](https://play.dhis2.org/), it seems to be used
at least in:

* `dataElements`
* `dataSets`
* `programs`


## Accessing google spreadsheets

To access the spreadsheets, you need a google *api key*. You can see
how to get one at the [google developers documentation
site](https://console.developers.google.com/apis/credentials).

The spreadsheet needs to have sharing permissions with, at least,
anyone that has the link to it (and not only anyone within your
organization). This is necessary because we use an api key to access
the spreadsheet, but we could avoid it if we used OAuth2
authentication (and in that case, we could also write on the
spreadsheet). It would be harder to implement, though, and seems
unnecessary.

If you want to understand the source code, these are some useful
resources for a quick start with the python api to google sheets:

* https://developers.google.com/sheets/api/quickstart/python
* https://stackoverflow.com/questions/44899425/


## Usage

```
usage: metasheets.py [-h] [--output OUTPUT] [--overwrite] [--api-key API_KEY]
                     [--sheet-id SHEET_ID] [--config CONFIG]
                     [--default-category-combo-id DEFAULT_CATEGORY_COMBO_ID]

Read google sheets with dhis2 metadata and create a json file that you can upload to a dhis2 instance.

options:
  -h, --help            show this help message and exit
  --output OUTPUT       output file (if empty, it is generated from the sheet id)
  --overwrite           do not check if the output file already exists
  --api-key API_KEY     api key to access google sheets
  --sheet-id SHEET_ID   id of the google sheet to read
  --config CONFIG       file with default parameters
  --default-category-combo-id DEFAULT_CATEGORY_COMBO_ID

```


## Metadata spreadsheet template

You can find an example spreadsheet to use as a template for
introducing metadata at [Metadata
template](https://docs.google.com/spreadsheets/d/1w3Vgf3oVIZO1-kxhe_ZG-O4rFoT9eZXtjvmChRY9PM8/).

Currently, these are the sheets it has and the columns on them:

```
dataSets                id, name, code, periodType, categoryCombo, description
sections                id, name, code, dataSet, showRowTotals, showColumnTotals, description
dataElements            id, name, shortName, dataSetSection, code, categoryCombo, valueType, aggregationType
programs                id, name, code, trackedEntityType, description
programStages           id, name, program, repeatable, description
programSections         id, name, programStage, description
trackedEntityTypes      id, name, description
trackedEntityAttributes id, name, shortName, formName, code, trackedEntityType, mandatory, displayInList
programDataElements     id, name, shortName, formName, programStageSections, code, valueType, aggregationType
categoryCombos          id, name, code, dataDimensionType, description
categories              id, name, shortName, code, categoryCombo, dataDimensionType, description
categoryOptions         id, name, code, category, shortName, description
optionSets              id, name, code, valueType, description
options                 id, name, code, optionSet, shortName, description

DHIS2                   Data element type, Data element aggregation, Data set period type
```

The last one is used to created named ranges to use as data validation
(and thus also get drop-down menus in google spreadsheets).


## Extending the script

### Adding new metadata

This script understands only a limited amount of all the metadata that
exist in dhis2.

You can extend it to add other kinds of metadata. For that, you will
need to understand how dhis2 represents those new kind of metadata.

To see what fields will be necessary, you can inspect with your
browser the api calls made when you add new metadata from the
`Maintenance` app.

Another option is to go to one of the servers at
https://play.dhis2.org/ and export its metadata with the
`Import/Export` app. Then, you can for example use `jq` to learn about
it. A few examples follow.

To see the keys present in the top-level object:

```sh
cat metadata.json | jq 'keys_unsorted'
```

To see the first in the list of programRules:

```sh
cat metadata.json | jq '.programRules[0]'
```

To see the paths to all the elements where "programRuleActions" is an
existing field:

```sh
cat metadata.json | jq -c 'paths | select(.[-1] == "programRuleActions")'
```

### Comparing metadata files

Comparing metadata json files is useful to see what can be missing or
wrong in the files.

For example, you can compare the files created by this script to
others created by a previous version, or another script, or with the
`Metadata export` option of the `Import/Export` app, or from
the `/api/metadata.json` endpoint.

To compare them, it is useful to have them properly sorted. The
following command helps to make them easy to compare:

```sh
cat metadata.json \
    | jq --sort-keys \
    | jq 'walk(if type == "array" then sort_by(.name) else . end)' \
    > metadata_sorted.json
```

For more information on how to use `jq`, see
https://stedolan.github.io/jq/manual/ .
